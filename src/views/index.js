import pc from 'paperclip'

export const HOME_VIEW = pc.template(require('./home.html'))
export const USER_INDEX_VIEW = pc.template(require('./user/index.html'))
export const USER_CREATE_VIEW = pc.template(require('./user/new.html'))
export const USER_SHOW_VIEW = pc.template(require('./user/show.html'))
export const USER_EDIT_VIEW = pc.template(require('./user/edit.html'))
