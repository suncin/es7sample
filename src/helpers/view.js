import router from './router'

export function renderContent(template, data = {}) {
  return template.view(data)
}

export function setContent(view) {
  const $containerApp = $('#app')
  $containerApp.html(view.render())
  router.updatePageLinks()
}
