import lf from 'lovefield'

const dbName = 'ipcDB'
const dbVersion = 1
const schemaBuilder = lf.schema.create(dbName, dbVersion)

/**
 * Definir el esquema para la tabla usuario
 */
schemaBuilder.createTable('User')
  .addColumn('id', lf.Type.INTEGER)
  .addColumn('firstname', lf.Type.STRING)
  .addColumn('lastname', lf.Type.STRING)
  .addColumn('email', lf.Type.STRING)
  .addColumn('hash', lf.Type.STRING)
  .addColumn('salt', lf.Type.STRING)
  .addColumn('createdAt', lf.Type.DATE_TIME)
  .addColumn('updatedAt', lf.Type.DATE_TIME)
  .addPrimaryKey(['id'])
  .addUnique('uniqueEmail', ['email'])
  .addIndex('idxFirstname', ['firstname'], false, lf.Order.DESC)
  .addIndex('idxLastname', ['lastname'], false, lf.Order.DESC)
  .addIndex('idxCreatedAt', ['createdAt'], false, lf.Order.DESC)
  .addIndex('idxUpdatedAt', ['updatedAt'], false, lf.Order.DESC)

/**
 * Obtener la conexión a la base de datos
 *
 * @export
 * @returns Promise<Object>
 */
export async function getDbConnection() {
  return schemaBuilder.connect()
}
