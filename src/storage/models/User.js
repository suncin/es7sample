import {getDbConnection} from '../db'
import {isString} from 'lodash'

const dbPromise = getDbConnection()
let db

dbPromise.then(instance => {
  db = instance
  return db
})

/**
 * Obtener el esquema para User de la base de datos
 *
 * @returns Promise<Object>
 */
async function getTable() {
  return dbPromise
  .then(() => db.getSchema().table('User'))
}

const User = {
  /**
   * Guardar una entidad a la base de datos
   *
   * @param {Object} user           La entidad a guardar
   * @param {String} user.firstname Los nombres del usuario
   * @param {String} user.lastname  Los apellidos del usuario
   * @param {String} user.email     El correo del usuario
   * @param {String} user.password  La contraseña del usuario
   * @returns Promise<Array>
   */
  async save({firstname, lastname, email, password}) {
    const userTable = await getTable()
    const row = userTable.createRow({
      id: Date.now(),
      firstname,
      lastname,
      email,
      hash: btoa(password),
      salt: Math.random().toString(16),
      createdAt: new Date(),
      updatedAt: new Date(),
    })
    return db.insertOrReplace()
    .into(userTable)
    .values([row])
    .exec()
  },
  /**
   * Buscar un usuario en la base de datos
   *
   * @param {int} id El identificador del usuario en particular
   * @returns Promise<Object>
   */
  async findOne(id) {
    const userTable = await getTable()
    return db.select()
    .from(userTable)
    .where(userTable.id.eq(id))
    .exec()
    .then(users => users[0])
  },
  /**
   * Listar los usuarios de la base de datos
   *
   * @returns Promise<Array>
   */
  async find() {
    const userTable = await getTable()
    return db.select()
    .from(userTable)
    .exec()
  },
  /**
   * Actualizar un usuario en la base de datos
   *
   * @param {int}    id             El identificador del usuario en particular
   * @param {Object} user           La entidad a guardar
   * @param {String} user.firstname Los nombres del usuario
   * @param {String} user.lastname  Los apellidos del usuario
   * @param {String} user.email     El correo del usuario
   * @param {String} user.password  La contraseña del usuario
   * @returns Promise<Array>
   */
  async update(id, {firstname, lastname, email}) {
    const userTable = await getTable()
    const query = db.update(userTable)
    let update = false
    if (isString(firstname)) {
      query.set(userTable.firstname, firstname)
      update = true
    }
    if (isString(lastname)) {
      query.set(userTable.lastname, lastname)
      update = true
    }
    if (isString(email)) {
      query.set(userTable.email, email)
      update = true
    }
    if (!update) {
      return Promise.resolve()
    }
    query.set(userTable.updatedAt, new Date()).where(userTable.id.eq(id))
    return query.exec()
  },
  /**
   * Eliminar un usuario de la base de datos
   *
   * @param {int} id El identificador del usuario en particular
   * @returns Promise
   */
  async delete(id) {
    const userTable = await getTable()
    return db.delete().from(userTable).where(userTable.id.eq(id)).exec()
  }
}

export default User
