import * as view from '../helpers/view'
import {USER_EDIT_VIEW} from '../views'
import User from '../storage/models/User'
import router from '../helpers/router'

function handleSubmit(user, event) {
  event.preventDefault()
  User.update(user.id, user)
  .then(() => router.navigate(`/user/${user.id}`))
  .catch(console.error)
}

async function handleDelete(id, event) {
  event.preventDefault()
  const dialog = document.querySelector('dialog')
  if (!dialog.showModal) {
    await import('dialog-polyfill/dialog-polyfill.css')
    const dialogPolyfill = await import('dialog-polyfill/dialog-polyfill.js')
    dialogPolyfill.registerDialog(dialog)
  }
  dialog.showModal()
  $('.js-stop', dialog).click(() => dialog.close())
  $('.js-doit', dialog).click(() => {
    User.delete(id)
    .then(() => router.navigate('/user'))
    .catch(console.error)
    dialog.close()
  })
}

export default async function UserEditController(params) {
  const user = await User.findOne(params.id)
  if (!user) {
    router.navigate('/user')
  }
  view.setContent(view.renderContent(USER_EDIT_VIEW, {user}))
  $('#userForm').on('submit', handleSubmit.bind(this, user))
  $('#btnDeleteUser').on('click', handleDelete.bind(this, params.id))
}
