import lfc from 'lodash-form-collector'
import * as view from '../helpers/view'
import {USER_CREATE_VIEW} from '../views'
import User from '../storage/models/User'
import router from '../helpers/router'

function handleSubmit(event) {
  event.preventDefault()
  const user = lfc(event.target)
  User.save(user).then(rows => {
    router.navigate(`/user/${rows[0].id}`)
  }).catch(console.error)
}

export default function UserCreateController() {
  view.setContent(view.renderContent(USER_CREATE_VIEW))
  $('#userForm').on('submit', handleSubmit)
}
