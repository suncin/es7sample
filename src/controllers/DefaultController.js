import * as view from '../helpers/view'
import {HOME_VIEW} from '../views'

export default function DefaultController() {
  view.setContent(view.renderContent(HOME_VIEW))
}
