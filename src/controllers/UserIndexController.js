import * as view from '../helpers/view'
import {USER_INDEX_VIEW} from '../views'
import User from '../storage/models/User'

export default async function UserIndexController() {
  const users = await User.find()
  view.setContent(view.renderContent(USER_INDEX_VIEW, {users}))
}
