import * as view from '../helpers/view'
import router from '../helpers/router'
import {USER_SHOW_VIEW} from '../views'
import User from '../storage/models/User'

export default async function UserShowController(params) {
  const user = await User.findOne(params.id)
  if (!user) {
    router.navigate('/user')
  }
  view.setContent(view.renderContent(USER_SHOW_VIEW, {user}))
}
