import 'material-design-lite/dist/material'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import './index.scss'
import router from './helpers/router'
import DefaultController from './controllers/DefaultController'
import UserIndexController from './controllers/UserIndexController'
import UserCreateController from './controllers/UserCreateController'
import UserShowController from './controllers/UserShowController'
import UserEditController from './controllers/UserEditController'

router.on({
  '/user': {
    as: 'user.index',
    uses: UserIndexController,
  },
  '/user/new': {
    as: 'user.new',
    uses: UserCreateController,
  },
  '/user/:id': {
    as: 'user.show',
    uses: UserShowController,
  },
  '/user/:id/edit': {
    as: 'user.edit',
    uses: UserEditController,
  }
})

router.on(DefaultController)

router.resolve()
