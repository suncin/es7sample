const webpack = require('webpack');
const conf = require('./gulp.conf');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const FailPlugin = require('webpack-fail-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const pkg = require('../package.json');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const BabiliPlugin = require('babili-webpack-plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

module.exports = {
  module: {
    loaders: [
      {
        test: /\.json$/i,
        loaders: [
          'json-loader'
        ]
      }, {
        test: /\.html$/i,
        loader: 'html-loader'
      }, {
        test: /\.js$/i,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        enforce: 'pre'
      }, {
        test: /\.css$/i,
        loaders: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {importLoaders: true}
            },
            'postcss-loader'
          ]
        })
      }, {
        test: /\.s[ac]ss$/i,
        loaders: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {importLoaders: true}
            },
            'postcss-loader',
            'sass-loader'
          ]
        })
      }, {
        test: /\.js$/i,
        exclude: /node_modules/,
        loaders: [
          'babel-loader'
        ]
      }, {
        test: /\.(jpg|jpeg|webp|png|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          },
          'img-loader'
        ]
      }, {
        test: /\.woff(\?v=\d+(\.\d+\.\d+)?)?$/i,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }, {
        test: /\.woff2(\?v=\d+(\.\d+\.\d+)?)?$/i,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }, {
        test: /\.[ot]tf(\?v=\d+(\.\d+\.\d+)?)?$/i,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }, {
        test: /\.eot(\?v=\d+(\.\d+\.\d+)?)?$/i,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }, {
        test: /\.svg(\?v=\d+(\.\d+\.\d+)?)?$/i,
        include: /font[s]?/i,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }
    ]
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    FailPlugin,
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new HtmlWebpackPlugin({
      template: conf.path.src('index.html')
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"'
    }),
    new LodashModuleReplacementPlugin(),
    new BabiliPlugin({
      removeConsole: true,
      removeDebugger: true
    }, {
      comments: false,
      sourceMap: true
    }),
    new webpack.optimize.UglifyJsPlugin({
      output: {comments: false},
      sourceMap: true,
      compress: {unused: true, dead_code: true, warnings: false, drop_console: true} // eslint-disable-line camelcase
    }),
    new ExtractTextPlugin('index-[contenthash].css'),
    new webpack.optimize.CommonsChunkPlugin({name: 'vendor'}),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: () => require('./postcss-dist.conf')
      }
    }),
    new StyleLintPlugin()
  ],
  devtool: 'hidden-source-map',
  output: {
    path: path.join(process.cwd(), conf.paths.dist),
    filename: '[name]-[hash].js'
  },
  entry: {
    app: `./${conf.path.src('index')}`,
    vendor: Object.keys(pkg.dependencies)
  },
  resolve: {
    extensions: ['.js', '.scss', '.css', '.json', '*'],
    alias: {
      lovefield: 'lovefield/dist/lovefield.es6.js',
    }
  }
};
