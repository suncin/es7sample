module.exports = {
  map: true,
  plugins: [
    require('postcss-import')(),
    require('postcss-url')(),
    require('postcss-inline-svg')(),
    require('postcss-font-magician')(),
    require('rucksack-css')({
      fallbacks: true,
      reporter: true
    }),
    require('postcss-short'),
    require('postcss-cssnext')({
      warnForDuplicates: false
    }),
    require('postcss-logical-props'),
    require('postcss-reporter')()
  ]
};
