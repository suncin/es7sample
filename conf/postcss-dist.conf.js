module.exports = {
  plugins: [
    require('postcss-import')(),
    require('postcss-url')(),
    require('postcss-inline-svg')(),
    require('rucksack-css')({
      fallbacks: true,
      reporter: true
    }),
    require('postcss-short'),
    require('postcss-cssnext')({
      warnForDuplicates: false
    }),
    require('postcss-logical-props'),
    require('css-mqpacker')(),
    require('postcss-mq-keyframes')(),
    require('cssnano')({
      discardComments: {
        removeAll: true
      },
      zindex: false
    }),
    require('postcss-reporter')()
  ]
};
