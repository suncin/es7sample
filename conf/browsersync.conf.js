const conf = require('./gulp.conf');

const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const jsonServer = require('json-server');

const webpackConf = require('./webpack.conf');
const webpackBundler = webpack(webpackConf);

const apiServer = jsonServer.create();
apiServer.use(jsonServer.defaults());
apiServer.use(jsonServer.router('db.json'));

module.exports = function () {
  return {
    server: {
      baseDir: [
        conf.paths.tmp,
        conf.paths.src
      ],
      middleware: [
        {
          route: '/api',
          handle: apiServer
        },
        webpackDevMiddleware(webpackBundler, {
          // IMPORTANT: dev middleware can't access config, so we should
          // provide publicPath by ourselves
          publicPath: webpackConf.output.publicPath,

          // Quiet verbose output in console
          quiet: true
        }),

        // Bundler should be the same as above
        webpackHotMiddleware(webpackBundler)
      ]
    },
    plugins: [
      'webpack-browser-sync-css-hmr'
    ],
    open: false
  };
};
