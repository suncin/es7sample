const webpack = require('webpack');
const conf = require('./gulp.conf');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const FailPlugin = require('webpack-fail-plugin');
const NpmInstallPlugin = require('npm-install-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = {
  module: {
    loaders: [
      {
        test: /\.json$/i,
        loaders: [
          'json-loader'
        ]
      }, {
        test: /\.html$/i,
        loader: 'html-loader'
      }, {
        test: /\.js$/i,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        enforce: 'pre'
      }, {
        test: /\.css$/i,
        loaders: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {importLoaders: true}
          },
          'postcss-loader'
        ]
      }, {
        test: /\.s[ac]ss$/i,
        loaders: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {importLoaders: true}
          },
          'postcss-loader',
          'sass-loader'
        ]
      }, {
        test: /\.js$/i,
        exclude: /node_modules/,
        loaders: [
          'babel-loader'
        ]
      }, {
        test: /\.(jpg|jpeg|webp|png|gif|svg)$/i,
        loader: 'file-loader',
        options: {
          name: 'images/[name].[ext]'
        }
      }, {
        test: /\.woff(\?v=\d+(\.\d+\.\d+)?)?$/i,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }, {
        test: /\.woff2(\?v=\d+(\.\d+\.\d+)?)?$/i,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }, {
        test: /\.[ot]tf(\?v=\d+(\.\d+\.\d+)?)?$/i,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }, {
        test: /\.eot(\?v=\d+(\.\d+\.\d+)?)?$/i,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }, {
        test: /\.svg(\?v=\d+(\.\d+\.\d+)?)?$/i,
        include: /font[s]?/i,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }
    ]
  },
  plugins: [
    new NpmInstallPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    FailPlugin,
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new HtmlWebpackPlugin({
      template: conf.path.src('index.html')
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: require('./postcss.conf')
      },
      debug: true
    }),
    new StyleLintPlugin()
  ],
  devtool: 'eval-source-map',
  output: {
    path: path.join(process.cwd(), conf.paths.tmp),
    filename: 'index.js'
  },
  entry: [
    'webpack/hot/dev-server',
    'webpack-hot-middleware/client',
    `./${conf.path.src('index')}`
  ],
  resolve: {
    extensions: ['.js', '.scss', '.css', '.json', '*'],
    alias: {
      lovefield: 'lovefield/dist/lovefield.es6.js',
    }
  }
};
